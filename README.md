#Projekt: Letalske povezave#

###Člani:###
* Anžej Curk
* Luka Matavž

###Programska orodja:###
* Python 3.X
* Knjižnica bottle
* postgreSQL (Psycopg2 package)
* Bootstrap
* Javascript
* Jquery

###Opis projekta:###
* Projekt pri predmetu Osnove podatkovnih baz v š.l. 2014/2015. Vsa koda se nahaja v mapi **src**.
* Podatki in opis podatkov, ki so preneseni v bazo so v mapi **podatki**.
* Za pravilno delovanje programa, je potrebno imeti `auth.py` datoteko, ki vsebuje uporabniško ime in geslo za dostop do baze. Zgradba datoteke izgleda tako:
```python
db = 'name_database'
host = 'baza.fmf.uni-lj.si'
user = 'username'
password = 'password'
```

###Vir podatkov:###
* [Openflights](http://openflights.org/data.html)
* [Ourairports](http://ourairports.com/)
* Wikipedia
* [Airline Route Mapper](http://arm.64hosts.com/)

###Shema podatkovne baze:###
![Baza](https://bitbucket.org/anzejcurk/opb-transport/raw/master/Baza.png)